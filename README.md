# SmartCities - SBC - UTPL

El presente código tiene: 

 - Archivo .owl del modelo ontologico para smartcities.
 - Archivo SmartCities.java para le generación de tripletas ubicado en **src/rdf/**
 - Archivo UrbanWork.java (frame) para la visualización de los datos, ubicado en **src/view/**.
 - Archivo urbanWork.csv con el dataset para las tripletas.
 - Archivo urbanWork.rdf con las tripletas ya generadas.

## Modelo en protegé

![enter image description here](https://lh3.googleusercontent.com/4xpN4NvdEIuukhCrJj6n4N8DTc39k5njyq6x766QrBmPmeOF8AycYO9YLX3K8zqjs5ewL0AEYJBWuw "Modelo Ontológico")

El modelo ontológico contiene Clases de **schema** y de **dbpedia** como se muestra en la imagen.
#### Propiedades de Objetos
 - dbo:commune
 - dbo:isPartOf
 - schema:address
 - schema:geo
#### Propiedades de Datos
 - dbo:description
 - dbo:type
 - dbo:amount
 - dbo:percentage
 - foaf:name
 - schema:endDate
 - schema:startDate
 - schema:longitude
 - schema:latitude
 - schema:streetAddress


## Generación de tripletas en Jena

En la generación de las tripletas se creó una Clase de tipo modelo llamada UrbanWork.java esto con la finalidad usar objetos usando **POO**.
Esta clase tiene los siguientes atributos y cada atributo tiene sus métodos set y get:

 -     String idUrbanWork;
 -     String name;
 -     String description;
 -     String amount;
 -     String percentage;
 -     String startDate;
 -     String endDate;
 -     String commune;
 -     String latitude;
 -     String longitude;
 -     String settlement;
 -     String streetAdrress;
 -     String type;
 
#### Método de lectura del modelo ontológico owl.

     //Metodo de obtención del modelo
    public static OntModel getOntologyModel(String ontoFile) {
        OntModel ontoModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);
        try {
            InputStream in = FileManager.get().open(ontoFile);
            try {
                ontoModel.read(in, null);
            } catch (Exception e) {
            }
            LOGGER.log(Level.INFO, "Ontology {0} cargada.", ontoFile);
        } catch (JenaException je) {
            System.err.println("ERROR" + je.getMessage());
            System.exit(0);
        }
        return ontoModel;
    }

#### Método de lectura del dataset

     //metodo de lectura del csv para cargar en el modelo
    public static ArrayList<UrbanWork> readDataset() {
        ArrayList<UrbanWork> urbanWorks = new ArrayList<>();
        UrbanWork urbanWork = null;
        try {InputStreamReader isr = new InputStreamReader(new FileInputStream("urbanWork.csv"),StandardCharsets.UTF_8 );
            CsvReader csvReader = new CsvReader(isr, ',');
            csvReader.readHeaders();
            while (csvReader.readRecord()) {
                if ("".equals(csvReader.get("monto_contrato")) || "".equals(csvReader.get("porcentaje_avance"))) {
                    LOGGER.log(Level.INFO, "Urban work [{0}] no cargada.", csvReader.get("id"));
                } else {
                    urbanWork = new UrbanWork();
                    urbanWork.setIdUrbanWork(csvReader.get("id"));
                    urbanWork.setName(csvReader.get("nombre"));
                    urbanWork.setDescription(csvReader.get("descripcion"));
                    urbanWork.setAmount(csvReader.get("monto_contrato"));
                    urbanWork.setPercentage(csvReader.get("porcentaje_avance"));
                    urbanWork.setStartDate(csvReader.get("fecha_inicio"));
                    urbanWork.setEndDate(csvReader.get("fecha_fin_inicial"));
                    urbanWork.setCommune(csvReader.get("comuna"));
                    urbanWork.setLatitude(csvReader.get("lat"));
                    urbanWork.setLongitude(csvReader.get("lng"));
                    urbanWork.setSettlement(csvReader.get("barrio"));
                    urbanWork.setStreetAdrress(csvReader.get("direccion"));
                    urbanWork.setType(csvReader.get("tipo"));
                }
                urbanWorks.add(urbanWork);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SmartCities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SmartCities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return urbanWorks;
    }

#### Método generador de tripletas.

    // Obtención del modelo creado en protegé
        OntModel model = getOntologyModel("obra.owl");
        Model newModel = ModelFactory.createDefaultModel();
        // creación del modelo 
        File f = new File("urbanWork.rdf");
        FileOutputStream os = new FileOutputStream(f);
        //Fijar Prefijo para URI base de dos datos a crear 
        String dataPrefix = "http://sbc.org/data/";
        model.setNsPrefix("data", dataPrefix);
        // Declaración de URIS para vocabularios que no están en Jena
        String urbanWorkURI = "http://www.charlie.com.ec/";
        String dbo = "http://dbpedia.org/ontology/";
        String dbp = "http://dbpedia.org/property/";
        String dbr = "http://dbpedia.org/resource/";
        String schema = "https://schema.org/";
        //Almacena el dataset del csv en un arrayList
        ArrayList<UrbanWork> datasetList = readDataset();
        // Recorremos el arraylist para realizar las tripletas
        for (UrbanWork urbanWork : datasetList) {
            
            Resource resourceUrbanWork = newModel.createResource(dataPrefix + urbanWork.getIdUrbanWork())
                    .addProperty(FOAF.name, urbanWork.getName())
                    .addProperty(model.getProperty(dbo + "description"), urbanWork.getDescription())
                    .addLiteral(model.getProperty(dbp + "amount"), Float.parseFloat(urbanWork.getAmount()))
                    .addLiteral(model.getProperty(dbp + "percentage"), Float.parseFloat(urbanWork.getPercentage()))
                    .addLiteral(model.getProperty(schema + "startDate"), urbanWork.getStartDate())
                    .addLiteral(model.getProperty(schema + "endDate"), urbanWork.getEndDate());

            Resource geoCoordinates = newModel.createResource(dataPrefix + "GeoCoordinates/" + urbanWork.getIdUrbanWork())
                    .addLiteral(model.getProperty(schema + "latitude"), Float.parseFloat(urbanWork.getLatitude()))
                    .addLiteral(model.getProperty(schema + "longitude"),urbanWork.getLongitude())
                    .addProperty(RDF.type, model.getResource(schema + "GeoCoordinates"));

            Resource postalAddress = newModel.createResource(dataPrefix + "PostalAddress/" + urbanWork.getIdUrbanWork())
                    .addLiteral(model.getProperty(schema + "streetAdrress"), urbanWork.getStreetAdrress())
                    .addProperty(RDF.type, model.getResource(schema + "PostalAddress"));

            Resource settlement = newModel.createResource(dataPrefix + "commune/" + urbanWork.getIdUrbanWork())
                    .addProperty(RDFS.label, urbanWork.getSettlement())
                    .addProperty(model.getProperty(dbo + "isPartOf"), model.getResource(dbr + "Argentina"))
                    .addProperty(model.getProperty(dbo + "isPartOf"), model.getResource(dbr + "Comuna_" + urbanWork.getCommune()));

            newModel.add(resourceUrbanWork, model.getProperty(dbo+"type"),urbanWork.getType())
                    .add(resourceUrbanWork, RDF.type, model.getResource(urbanWorkURI + "UrbanWork"))
                    .add(resourceUrbanWork, model.getProperty(schema + "geo"), geoCoordinates)
                    .add(resourceUrbanWork, model.getProperty(dbo + "commune"), settlement)
                    .add(geoCoordinates, model.getProperty(schema + "address"), postalAddress);
        }
        //newModel.write(System.out, "RDF/XML");
        RDFWriter writer = model.getWriter("RDF/XML");
        writer.write(newModel, os, "");
        LOGGER.log(Level.INFO, "Tripletas generadas correctamente.");


## App Java (Muestra de datos)

Para la muestra de los datos se creo una pequeña vista que permite realizar lo siguiente:

 1. Ingresar el host de virtuoso, ejm (`http://localhost:8890/sparql`).
 2. Ingresar la URI del grafo creado en virtuoso  ejm (`http://data.smartcities.com.ec/dataset/`)
 3. Seleccionar el tipo de obra Urbana ejm (`Escuela, Hospital, etc`)
 4. Seleccionar el nombre de la obra Urbana para que se muestre en la App los datos como: `monto, descripción, fecha de inicio y fin, dirección`
 5. Existe un botón que abre ***Google Maps*** para poder visualizar donde se encuentra la obra.

A continuación se presenta capturas de pantalla con la funcionalidad del aplicativo.

 - Se selecciona un tipo de obra Urbana y se da click en el botón mostrar.

![enter image description here](https://lh3.googleusercontent.com/SwYfKTdL6D5-ADx__GZLPTe2MURb7rA1R7wwCK_0goTxILeXOjRBSMiwNcZne9FkBdWTIGWSbYB1MA "Captura 1")

 - Al realizar el paso anterior se muestra los siguientes datos en este caso se seleccionó el tipo **Vivivienda**.

![enter image description here](https://lh3.googleusercontent.com/Hsa6iyM6HasWmpvQ_LwUdUuALNC847TQXWctBUliJy00zNvFY534WqVvPXBJXKyBWmHJEQcPl5ZLUw "Paso 2")

 - Luego seleccionamos el nombre de una **Vivienda** en este caso se selecciona ***Villa Olimpica: Vivienda V*** para que se muestren los datos al lado derecho, como se visualiza en la siguiente gráfica.

![enter image description here](https://lh3.googleusercontent.com/xz-k-UdLpKGniCacxea9-aodDjodAtfqpv689APOIkyilL_WwmTNpG2BJAB2fXSpuky_UuQopO4TCg "Paso 3")

## Nota
Si se presiona el botón de Google Maps, la aplicación abrirá tu navegador e irá a las coordenadas en donde se encuentra dicha obra.

![enter image description here](https://lh3.googleusercontent.com/uzA3wa7oxmI8cD5YrvSV30GS_J3Xh4nGQzHpJQe6JhyXILkF1Q_fKOBf1LDd4zLHp6t1oUBnwhfqHQ "Paso 4")

**Participantes del proyecto**

 - [Charlie Cárdenas Toledo](cacardenas7@utpl.edu.ec)
 - [Danilo León](drleon@utpl.edu.ec)