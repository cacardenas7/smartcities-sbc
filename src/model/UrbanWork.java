package model;

/**
 *
 * @author cacar
 */
public class UrbanWork {

    String idUrbanWork;
    String name;
    String description;
    String amount;
    String percentage;
    String startDate;
    String endDate;
    String commune;
    String latitude;
    String longitude;
    String settlement;
    String streetAdrress;
    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getStreetAdrress() {
        return streetAdrress;
    }

    public void setStreetAdrress(String streetAdrress) {
        this.streetAdrress = streetAdrress;
    }

    public String getSettlement() {
        return settlement;
    }

    public void setSettlement(String settlement) {
        this.settlement = settlement;
    }

    //String 
    public UrbanWork() {
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getIdUrbanWork() {
        return idUrbanWork;
    }

    public void setIdUrbanWork(String idUrbanWork) {
        this.idUrbanWork = idUrbanWork;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
