package rdf;

import com.csvreader.CsvReader;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.UrbanWork;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.RDFWriter;
import org.apache.jena.shared.JenaException;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.RDFS;
/**
 *
 * @author IngCharlie7
 */
public class SmartCities {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        // Obtención del modelo creado en protegé
        OntModel model = getOntologyModel("obra.owl");
        Model newModel = ModelFactory.createDefaultModel();
        // creación del modelo 
        File f = new File("urbanWork.rdf");
        FileOutputStream os = new FileOutputStream(f);
        //Fijar Prefijo para URI base de dos datos a crear 
        String dataPrefix = "http://sbc.org/data/";
        model.setNsPrefix("data", dataPrefix);
        // Declaración de URIS para vocabularios que no están en Jena
        String urbanWorkURI = "http://www.charlie.com.ec/";
        String dbo = "http://dbpedia.org/ontology/";
        String dbp = "http://dbpedia.org/property/";
        String dbr = "http://dbpedia.org/resource/";
        String schema = "https://schema.org/";
        //Almacena el dataset del csv en un arrayList
        ArrayList<UrbanWork> datasetList = readDataset();
        // Recorremos el arraylist para realizar las tripletas
        for (UrbanWork urbanWork : datasetList) {
            
            Resource resourceUrbanWork = newModel.createResource(dataPrefix + urbanWork.getIdUrbanWork())
                    .addProperty(FOAF.name, urbanWork.getName())
                    .addProperty(model.getProperty(dbo + "description"), urbanWork.getDescription())
                    .addLiteral(model.getProperty(dbp + "amount"), Float.parseFloat(urbanWork.getAmount()))
                    .addLiteral(model.getProperty(dbp + "percentage"), Float.parseFloat(urbanWork.getPercentage()))
                    .addLiteral(model.getProperty(schema + "startDate"), urbanWork.getStartDate())
                    .addLiteral(model.getProperty(schema + "endDate"), urbanWork.getEndDate());

            Resource geoCoordinates = newModel.createResource(dataPrefix + "GeoCoordinates/" + urbanWork.getIdUrbanWork())
                    .addLiteral(model.getProperty(schema + "latitude"), Float.parseFloat(urbanWork.getLatitude()))
                    .addLiteral(model.getProperty(schema + "longitude"),urbanWork.getLongitude())
                    .addProperty(RDF.type, model.getResource(schema + "GeoCoordinates"));

            Resource postalAddress = newModel.createResource(dataPrefix + "PostalAddress/" + urbanWork.getIdUrbanWork())
                    .addLiteral(model.getProperty(schema + "streetAdrress"), urbanWork.getStreetAdrress())
                    .addProperty(RDF.type, model.getResource(schema + "PostalAddress"));

            Resource settlement = newModel.createResource(dataPrefix + "commune/" + urbanWork.getIdUrbanWork())
                    .addProperty(RDFS.label, urbanWork.getSettlement())
                    .addProperty(model.getProperty(dbo + "isPartOf"), model.getResource(dbr + "Argentina"))
                    .addProperty(model.getProperty(dbo + "isPartOf"), model.getResource(dbr + "Comuna_" + urbanWork.getCommune()));

            newModel.add(resourceUrbanWork, model.getProperty(dbo+"type"),urbanWork.getType())
                    .add(resourceUrbanWork, RDF.type, model.getResource(urbanWorkURI + "UrbanWork"))
                    .add(resourceUrbanWork, model.getProperty(schema + "geo"), geoCoordinates)
                    .add(resourceUrbanWork, model.getProperty(dbo + "commune"), settlement)
                    .add(geoCoordinates, model.getProperty(schema + "address"), postalAddress);
        }
        //newModel.write(System.out, "RDF/XML");
        RDFWriter writer = model.getWriter("RDF/XML");
        writer.write(newModel, os, "");
        LOGGER.log(Level.INFO, "Tripletas generadas correctamente.");

    }

    //Metodo de obtención del modelo
    public static OntModel getOntologyModel(String ontoFile) {
        OntModel ontoModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);
        try {
            InputStream in = FileManager.get().open(ontoFile);
            try {
                ontoModel.read(in, null);
            } catch (Exception e) {
            }
            LOGGER.log(Level.INFO, "Ontology {0} cargada.", ontoFile);
        } catch (JenaException je) {
            System.err.println("ERROR" + je.getMessage());
            System.exit(0);
        }
        return ontoModel;
    }

    //metodo de lectura del csv para cargar en el modelo
    public static ArrayList<UrbanWork> readDataset() {
        ArrayList<UrbanWork> urbanWorks = new ArrayList<>();
        UrbanWork urbanWork = null;
        try {InputStreamReader isr = new InputStreamReader(new FileInputStream("urbanWork.csv"),StandardCharsets.UTF_8 );
            CsvReader csvReader = new CsvReader(isr, ',');
            csvReader.readHeaders();
            while (csvReader.readRecord()) {
                if ("".equals(csvReader.get("monto_contrato")) || "".equals(csvReader.get("porcentaje_avance"))) {
                    LOGGER.log(Level.INFO, "Urban work [{0}] no cargada.", csvReader.get("id"));
                } else {
                    urbanWork = new UrbanWork();
                    urbanWork.setIdUrbanWork(csvReader.get("id"));
                    urbanWork.setName(csvReader.get("nombre"));
                    urbanWork.setDescription(csvReader.get("descripcion"));
                    urbanWork.setAmount(csvReader.get("monto_contrato"));
                    urbanWork.setPercentage(csvReader.get("porcentaje_avance"));
                    urbanWork.setStartDate(csvReader.get("fecha_inicio"));
                    urbanWork.setEndDate(csvReader.get("fecha_fin_inicial"));
                    urbanWork.setCommune(csvReader.get("comuna"));
                    urbanWork.setLatitude(csvReader.get("lat"));
                    urbanWork.setLongitude(csvReader.get("lng"));
                    urbanWork.setSettlement(csvReader.get("barrio"));
                    urbanWork.setStreetAdrress(csvReader.get("direccion"));
                    urbanWork.setType(csvReader.get("tipo"));
                }
                urbanWorks.add(urbanWork);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SmartCities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SmartCities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return urbanWorks;
    }
}
